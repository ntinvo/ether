# When Was Our Smart Contract Deployed?
An app that takes a contract address, a host and returns the block hash and transaction hash rom which a contract address was deployed.

## Test the Result:
* Clone the repo: ``` git clone https://tinnvo@bitbucket.org/tinnvo/ether.git ```

* Run: ``` python3 main.py <contract_address> --host https://mainnet.infura.io/<API> ```

* Options:

  ```

  Usage:
  python3 ether.py <0xcontract_address> --host https://host.com/<API_SECRET>

  Options and arguments:
  <0xcontract_address>         : address of the smart contract
  --host                       : the host name
  --p                          : show the search progress

  Example to run the search with the progress:
  python3 ether.py <0xcontract_address> --host https://mainnet.infura.io/<API_SECRET> --p

  ```

## How does it work?
  The program does a binary search on the entire block chain and search for the block where the given contract address was deployed.

  For a given address and a block number

  * If that block number is the block where what contract was deployed or not, if this is the case then we can return the block number hash and the transaction hash right away

  * If that block is not the block when the contract was deployed, then it will check if the contract address exist in that block or not using ```Eth.getCode()```. By "exists", I meant, the contract was created on that block or the block before that block. If this is the case, we just keep on going to the left of the block chain until we find the block where the contract was deployed

  * And if the contract does not exist in the giving block, then we just go to the right of the block chain and do the search

## Challenges?
  This problem is kind of hard for me because this is the very first time I work with Web3.

  * First try (works but slow - real slow): I just think that I can simply just do a linear on the entire block chain, and then find the block where the contract was deployed and print out the hashes. Soon enough, I realized that I can't do that because it will take days, weeks, years, centuries (not that long but you know what I meant). So, I can't choose this one.

  * Second try (works and fast - real fast but it does not satisfy the challenge requirements): I was thinking of using the existing API (like EtherScan) to fetch all of the transactions that belong to the contract address, and from there I can get the hashes. However, this it does not satisfy the challenge requirements, so I can't choose this one either.

  * Third try (work and fast but not as fast as second try of course): I did the binary search on the block chain as what I mentioned above. I go with this one.

## Improvements?
  Even though the performance is better, but it is still pretty slow to me. If possible I want to improve its performance more so that it will be faster in the future (Fourth try?  maybe? <working in progress>)

  The records can be store in a cache somewhere, so we only perform the search when the contract address is not in the cache. Those can be stored in a database (MLab for example). Something like this will be stored:

  ```
  {
    "address" : <contract_address>,
    "init_block_hash" : <initial block hash>,
    "init_transaction_hash" : <initial transaction hash>,
    "block_number" : <block number>
  }
  ```

## Built With

* [Python](https://www.python.org/download/releases/3.0/) - a programming language that lets you work more quickly and integrate your systems more effectively.
* [Web3.py](https://web3py.readthedocs.io/en/stable/index.html) - a python library for interacting with Ethereum.


## Authors

* **Tin Vo** - *Initial work* - [Tin Vo](https://tinnvo.github.io/)
