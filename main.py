import sys
import requests
from web3 import Web3, HTTPProvider

def helper():
    '''
    This is a helper function that prints the help menu
    '''
    print('Usage: ')
    print('python3 ether.py <0xcontract_address> --host https://host.com/<API_SECRET>')
    print()
    print('Options and arguments: ')
    print('<0xcontract_address>         : address of the smart contract')
    print('--host                       : the host name')
    print('--p                          : show the search progress')
    print()
    print('Example: ')
    print('python3 ether.py <0xcontract_address> --host https://mainnet.infura.io/<API_SECRET>')

def get_web3_obj(provider_url):
    '''
    This functions returns the Web3 object from the provided url

    Args:
        provider_url: the provided url

    Returns:
        Returns the Web3 object
    '''
    return Web3(HTTPProvider(provider_url))

def get_args():
    '''
    This function checks the arguments passed to the program, the program will print print out the help menu and
    exit if the number of required arguments are not passed. It will also check for the flag --p if the user wants
    to see the progress of the search

    Returns:
        Returns contract address, host, and show_progress flag
    '''
    args = sys.argv
    if len(args) < 4:
        helper()
        sys.exit()
    return args[1], args[3], '--p' in args

def validate_args(address, web3):
    '''
    Simple function to validate the contract address and the host

    Args:
        address: the contract address
        web3: Web3 object
    '''
    try:
        web3.toHex(web3.eth.getCode(address))
    except Exception as e:
        print(e)
        sys.exit()

def get_current_block_number(web3):
    '''
    This functions returns the number of the most recent block from the passed web3 object

    Args:
        web3: Web3 object

    Returns:
        Returns the number of the most recent block
    '''
    currrent_block_number = web3.eth.blockNumber
    while currrent_block_number is None:
        currrent_block_number = web3.eth.blockNumber
    return currrent_block_number


def get_transactions_by_block(web3, block_number):
    '''
    This function returns the list of transactions that belong to the given block number. Based on the indices, this
    function goes throgh all of the transactions, get the transaction receipt, add them to the list, and returns

    Args:
        web3: Web3 object
        block_number: the block chain number

    Returns:
        Returns a list of transactions that belong to the given block number
    '''
    transactions = []

    # get the total number of transactions within the given block number
    num_of_transactions = web3.eth.getBlockTransactionCount(block_number)
    while num_of_transactions is None:
        num_of_transactions = web3.eth.getBlockTransactionCount(block_number)

    # go through all of the transactions, get their transaction receipts, and add them to the list
    for index in range(num_of_transactions):
        transaction = web3.eth.getTransactionFromBlock(block_number, index)
        while transaction is None:
            transaction = web3.eth.getTransactionFromBlock(block_number, index)
        transactionReceipt = web3.eth.getTransactionReceipt(web3.toHex(transaction.hash))
        while transactionReceipt is None:
            transactionReceipt = web3.eth.getTransactionReceipt(web3.toHex(transaction.hash))
        transactions.append(transactionReceipt)
    return transactions

def contract_was_created(web3, address, currrent_block_number):
    '''
    This function uses the byte_code to check if the given contract address was created before or not. If
    the byte_code is '0x' this means that the contract was not created, so it will return False; True otherwise

    Args:
        web3: Web3 object
        address: the contract address
        currrent_block_number: the current block chain number

    Returns:
        Returns if the contract was created or not
    '''
    # get the byte code and check if given contract address was created before or not
    byte_code = web3.toHex(web3.eth.getCode(address, currrent_block_number))
    while byte_code is None:
        byte_code = web3.toHex(web3.eth.getCode(address, currrent_block_number))
    return byte_code != '0x'

def contract_exists_in_block(web3, address, currrent_block_number):
    '''
    This functions check if the given contract address was created in the given block number or not. It
    will check if the contract was created before or not. This will be call when the program performs
    the binary search on the block chain

    This functions return 3 values:
        - First returned value: this bool value indicates of the address exist in the given block number or not
        - Second returned value: the transaction hash
        - Third returned value: the block hash

    # NOTE: for the second and third returned values, if the conctract was CREATED in the current block number
    then those will be set the the transaction hash and block hash respectively (this means that we found the
    block when the contract was deployed); otherwise, those will be set to None

    Args:
        web3: Web3 object
        address: the contract address
        currrent_block_number: the current block chain number

    Returns:
        This functions return 3 values: <exists/created in block>, <transaction hash>, <block hash>
    '''

    # the contract does not exist in the current block number
    if not contract_was_created(web3, address, currrent_block_number):
        return False, None, None

    # get the list of transactions
    transactions = get_transactions_by_block(web3, currrent_block_number)

    # we found the block when the contract was deployed, return the results
    for transaction in transactions:
        if 'contractAddress' in transaction and transaction.contractAddress == address:
            return True, web3.toHex(transaction.transactionHash), web3.toHex(transaction.blockHash)

    # if we get to this point, this means that the contract exist, but the current block is not the block
    # when the given contract was created (or deployed)
    return True, None, None

def search_for_address(address, web3, starting_block_number, ending_block_number, show_progress=False):
    '''
    This function performs the binary seach on the block chain

    Args:
        address: the contract address
        web3: Web3 object
        starting_block_number: the very first block of the block chain
        currrent_block_number: the last (current) block number
        show_progress: show the search progress or not
    '''
    # base case of the binary search
    if starting_block_number > ending_block_number:
        return

    # get the 'mid' index
    currrent_block_number = int(starting_block_number + (ending_block_number - starting_block_number) / 2)

    # show the search progress or not based on the flag
    if show_progress is True:
        print('Searching in block # ' + str(currrent_block_number))

    # check if the contract address exists in the current block number or not
    exists_in_block, tran_hash, block_hash = contract_exists_in_block(web3, address, currrent_block_number)

    # if we found the current block is the block when the contract was deployed then it will print out the result and exit
    # if the contract not exists, then go to the right of the block chain, go left otherwise
    if (exists_in_block is True) and (tran_hash is not None) and (block_hash is not None):
        print('Block: ' + block_hash)
        print('Transaction: ' + tran_hash)
        sys.exit()
    elif (exists_in_block is True) and (tran_hash is None) and (block_hash is None):
        search_for_address(address, web3, starting_block_number, currrent_block_number - 1, show_progress)
    else:
        search_for_address(address, web3, currrent_block_number + 1, ending_block_number, show_progress)

def main():
    '''
    The driver function that will call the functions to check the arguments, get the web3 object, and perform
    the binary search on the ethereum block chain
    '''
    # get the arguments and web3 object
    address, host, show_progress = get_args()
    web3 = get_web3_obj(host)

    # validate arguments
    validate_args(address, web3)

    # the get latest block number and perform the search
    currrent_block_number = get_current_block_number(web3)
    search_for_address(address, web3, 0, currrent_block_number, show_progress)

if __name__ == '__main__':
    main()
